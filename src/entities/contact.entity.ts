import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export default class ContactEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  email: string;

  @Column()
  phone: string;
  
  @Column()
  city: string;

  @Column()
  country: string;  
}