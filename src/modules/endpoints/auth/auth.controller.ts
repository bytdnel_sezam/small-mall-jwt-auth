import { Controller, Post, Body } from '@nestjs/common';
import { AuthService, AuthCredentials, loginCredentials } from './auth.service';
// import UserEntity from '../../../entities/user.entity';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/sendAuthenticationCode')
  async sendAuthenticationCode(
    @Body() credentials: AuthCredentials,
  ): Promise<any> {
    return this.authService.sendAuthenticationCode(credentials);
  }

  @Post('login')
  async login(@Body() loginData: loginCredentials): Promise<any> {
    return this.authService.login(loginData);
  }
}
