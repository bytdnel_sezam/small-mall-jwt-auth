import BcryptService from '../../services/bcryptjs/bcryptjs.service';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import UserEntity from '../../../entities/user.entity';
import { UserService } from './user.service';

export interface AuthCredentials {
  firstName: string;
  phoneNumber: string;
}

interface PhoneValidation {
  phoneValidationId?: string;
  error?: string;
}

type Code = string;

export interface loginCredentials {
  user: UserEntity;
  phoneValidationId: string;
  code: Code;
}

// enum TimeInMs {
//     Hours24 = 24*60*60*1000,
// }

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly bcryptService: BcryptService,
  ) {}

  // private async _getUserByPhone(phone: string): Promise<UserEntity> {
  //   const user = await this.userService.findByPhoneNumber(phone);
  //   return user;
  // }

  private _PHONE_VALIDATION_ID = '123456789';

  private _compareUserAndCredentials(
    user: UserEntity,
    credentials: AuthCredentials,
  ): boolean {
    let phoneNumberMatch = false;
    let firstNameMatch = false;
    if (user) {
      phoneNumberMatch = user.phoneNumber == credentials.phoneNumber;
      firstNameMatch = user.firstName == credentials.firstName;
    }

    console.log(phoneNumberMatch && firstNameMatch);

    return phoneNumberMatch && firstNameMatch;
  }

  private async _recognizeUser(credentials: AuthCredentials): Promise<boolean> {
    const userInstance = await this.userService.findByPhoneNumber(
      credentials.phoneNumber,
    );
    console.log({ userInstance });
    const userRecognized = this._compareUserAndCredentials(
      userInstance,
      credentials,
    );

    if (userRecognized) {
      return true;
    } else {
      return false;
    }
  }

  private async _sendCodeToPhone(phoneNumber: string): Promise<string> {
    return this._PHONE_VALIDATION_ID;
  }

  // private async _validateUser(credentials: AuthCredentials): Promise<any> {
  //   let passwordSuitable: boolean;
  //   const authenticatedUser:
  //     | UserEntity
  //     | false = await this.userService.findByEmail(credentials.email);

  //   if (authenticatedUser) {
  //     passwordSuitable = await this.bcryptService.compare(
  //       credentials.password,
  //       authenticatedUser.password,
  //     );
  //   }
  //   if (authenticatedUser && passwordSuitable) return authenticatedUser;
  //   else return false;
  // }

  private async _getAccessToken(user: UserEntity): Promise<string> {
    const userStringified = JSON.stringify(user);
    const accessToken = await this.jwtService.signAsync(userStringified);
    return accessToken;
  }

  private async _validatePhoneIdByCode(
    phoneValidationId: string,
    code: string,
  ): Promise<boolean> {
    if (phoneValidationId == this._PHONE_VALIDATION_ID) {
      return true;
    } else {
      return false;
    }
  }

  async sendAuthenticationCode(
    credentials: AuthCredentials,
  ): Promise<PhoneValidation> {
    // const userExists = await this._recognizeUser(credentials);
    const phoneValidationId: string = await this._sendCodeToPhone(
      credentials.phoneNumber,
    );

    if (phoneValidationId) {
      return {
        phoneValidationId,
      };
    } else {
      return {
        error: 'Failed to send code.',
      };
    }
  }

  async login(loginData: loginCredentials) {
    const phoneConfirmed = await this._validatePhoneIdByCode(
      loginData.phoneValidationId,
      loginData.code,
    );

    if (phoneConfirmed) {
      const userExists = await this._recognizeUser(loginData.user);
      console.log({ userExists });
      let userInstance: UserEntity;
      if (!userExists) {
        userInstance = await this.userService.create(loginData.user);
      } else {
        userInstance = await this.userService.findByPhoneNumber(
          loginData.user.phoneNumber,
        );
      }

      return {
        userInstance,
        accessToken: await this._getAccessToken(userInstance),
      };
    } else {
      return {
        error: 'Failed to login user',
      };
    }
  }

  // private async decodeJWT(accessToken: string) {
  //     return await this.jwtService.verifyAsync(accessToken);
  // }

  // public async login(
  //   credentials: AuthCredentials,
  // ): Promise<any | { status: number }> {
  //   const userInstance = await this._validateUser(credentials);
  //   if (userInstance) {
  //     const accessToken = await this._getAccessToken(userInstance);
  //     return {
  //       access_token: accessToken,
  //       status: 200,
  //     };
  //   } else {
  //     return { status: 404 };
  //   }
  // }

  // public async register(user: UserEntity): Promise<any> {
  //   const userInstance = await this._validatePhone(user.phoneNumber);
  //   console.log({ userInstance });
  //   if (userInstance) {
  //     return {
  //       status: 500,
  //     };
  //   } else {
  //     const hashedPassword = await this.bcryptService.hash(user.password);
  //     user.password = hashedPassword;
  //     const userCreated = await this.userService.create(user);
  //     const accessToken = await this._getAccessToken(userCreated);
  //     return {
  //       access_token: accessToken,
  //       status: 200,
  //     };
  //   }
  // }
}
